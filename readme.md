# AI Mindmap Generator

A set of GCP cloud functions which use AI to generate educational mindmaps based on input criteria. See the [specification](docs/spec.md) for more information.

## Architecture
See [architecture](docs/architecture.md).

## Scaling Considerations
See alternate architecure above.

- ChatGPT caching
  - use cached data if it exists for a given subject-topic pairing
  - otherwise continue to ChatGPT and add the result to the cache
- A job queue for handling many simultaneous requests to the functions
- pagination for the `mindmap-list` function

## Functions


### mindmap-list

#### Description
A simple API which queries the database for mindmap entries and returns them as JSON.

#### Deployment
Done via the gcloud CLI tool. Project must be built first using `npm run build`.

```
gcloud functions deploy mindmap-list \
--gen2 \
--runtime=nodejs20 \
--region=europe-west3 \
--trigger-http
```
### mindmap-process
#### Description
Triggered by files added to the `mindmap-input` storage bucket.

Downloads and parses the file before passing the data over to ChatGPT for mindmap generation.

The ChatGPT format instructions specify that the mindmap be formatted in a flat tree structure, i.e. an array of nodes which have a reference to a parent node. Additionally, a mermaid diagram is included so that any consumer can easily display the diagram.

An output CSV file is created indicating the status of mindmap creation. This is uploaded to the `mindmap-output` storage bucket.

The mindmap data itself is uploaded to the cloud database.



#### Deployment
Done via the gcloud CLI tool. Project must be built first using `npm run build`.
```
gcloud functions deploy mindmap-process \
--gen2 \
--runtime=nodejs20 \
--region=europe-west3 \
--trigger-bucket=mindmap-input \
--set-env-vars OUTPUT_BUCKET_NAME=mindmap-output \
--set-secrets 'OPENAI_API_KEY=OPENAI_API_KEY:latest'
```

## TODO
- tests
- CI/CD
- improve AI prompt for more consistent data
- improve input validation
- handle multiple files
