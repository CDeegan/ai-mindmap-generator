# Architecture
![Architecture Diagram](architecture-actual.png "Architecture Diagram")

## Components

### Cloud Functions
Lightweight serverless functions ideal for handling the mindmap generation process. Adding a CSV to the `mindmap-input` bucket triggers the `mindmap-process` function, which outputs data to the `mindmap-output` bucket and the Firestore database's `mindmap` collection. The `mindmap-list` function is triggered on HTTP request, where it reads from the same Firestore database.

[Go to Admin Panel](https://console.cloud.google.com/functions/list?hl=en&project=take-home-assignment-ciaran)

### Storage Buckets
Used to store the input and output CSV files of the mindmap generation function.

[Go to Admin Panel](https://console.cloud.google.com/storage/browser?hl=en&project=take-home-assignment-ciaran)

### Firestore
This NoSQL cloud database was used to handle the relatively unstructured nature of ChatGPT responses. It includes a `mindmap` collection which stores mindmap data. Documents in this collection can be inserted and updated.

[Go to Admin Panel](https://console.cloud.google.com/firestore/databases?hl=en&project=take-home-assignment-ciaran)

## Alternate Architecture
![Architecture Diagram](architecture-proposed.png "Architecture Diagram")