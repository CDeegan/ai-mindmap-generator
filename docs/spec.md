# Background

After LLMs (large language models) proved to be able to deliver high quality results for content generation, we’re looking for ways to use them for automated content generation.

 

This assignment’s scope is very close to the real problems we once tried to solve in the backend team and we hope you’ll be excited to work on it to get a grasp about the topics similar to what we work on.

# Problem

Your team wants to evaluate LLMs for generating [Mind Maps](https://en.wikipedia.org/wiki/Mind_map) and you support them with that.

# Goal

Your goal is to build an environment for generating a mind map based on input data (context) defined in the [input CSV](input_context_v2.csv) file with the help of OpenAI’s GPT3.5-Turbo API.

 

Once generated, mind maps must be stored in a storage of your choice inside of GCP with a reference to the subject and topic they were generated for.

 

The actual status of generation must also be written to the corresponding column inside of the output CSV file which is being generated after the generation is completed. Following statuses must be covered:

-   Success: status showing that mind map generation has been completed and successful.
   
-   Failure: status showing that mind map generation has been completed but an error occurred.
   

 

Make the output CSV file to contain only two columns: “topic” and “status” (so that it’s clear if generation has been run successfully for a particular topic).

 

Finally, the API which allows users to consume generated mind maps must be implemented.

 

The API response must return the valid JSON payload containing the collection of all generated mind maps along with their corresponding subject and topic names in the data structure of your choice. Hint: the API consumer wants to use information retrieved from API for building a UI which shows a mind map content along with the Subject and Topic names it belongs to.

# LLM prompt

Use the following prompt when interacting with GPT3.5-Turbo and populate variables inside of the prompt with values from corresponding columns inside of the [input CSV sheet](input_context_v2.csv). Feel free to use default values for model hyperparameters.

 

For the {{ mind_map_data_structure }} variable, define the JSON data structure yourself to your best knowledge about what a typical mind map should contain.

 
```
You are a professional teacher in {{ subject }}.

Your goal is to generate a mind map for the subject above with the focus on the {{ topic }} so that a student can improve their understanding of {{ subject }} and {{ topic }} while using that mind map.

The mind map should feature sub-topics of the {{ topic }} and no other content.

The result of your work must be a mind map in the form of JSON using the following data structure:

{{ mind_map_data_structure }}
```



# Other considerations

-   Ignore the content quality aspect of the generated mind map.
   
-   While the scope of the task may seem big, consider submitting the assignment which addresses the goals at a minimum level. If you would like to spend more time and showcase your skills, feel free to do so.
   
-   Be ready to walk us through your ideas about how to cover corner cases and potential scaling scenarios: be it in code you deliver or in-person later during the interview.