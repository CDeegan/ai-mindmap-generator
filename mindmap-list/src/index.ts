import { http, Request, Response } from '@google-cloud/functions-framework';
import { listAllInCollection } from './db';

const DB_COLLECTION_NAME = 'mindmap';
http('mindmap-list', async (req: Request, res: Response) => {
  const response = await listAllInCollection(DB_COLLECTION_NAME);
  res.json(response);
});
