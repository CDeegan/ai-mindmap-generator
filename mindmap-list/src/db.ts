import { Firestore } from '@google-cloud/firestore';

const db = new Firestore();

export async function listAllInCollection(collection: string) {
  const snapshot = await db.collection(collection).get();
  return snapshot.docs.map((doc) => doc.data());
}
