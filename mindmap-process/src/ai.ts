import { InputValues } from '@langchain/core/utils/types';
import { ChatPromptTemplate } from '@langchain/core/prompts';
import { ChatOpenAI } from '@langchain/openai';
import { StructuredOutputParser } from 'langchain/output_parsers';
import { config, MindMapResponse } from './config/mindmap-ai';

const model = new ChatOpenAI({
  openAIApiKey: process.env.OPENAI_API_KEY
});

const parser = StructuredOutputParser.fromNamesAndDescriptions(config.parser);

const prompt = ChatPromptTemplate.fromMessages(config.prompt as unknown as ChatPromptTemplate<InputValues, string>[]);

export async function generateMindmap(subject: string, topic: string): Promise<MindMapResponse> {
  return (await prompt.pipe(model).pipe(parser).invoke({
    subject,
    topic,
    format_instructions: parser.getFormatInstructions()
  })) as unknown as MindMapResponse;
}
