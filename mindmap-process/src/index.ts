import { cloudEvent } from '@google-cloud/functions-framework';
import { StorageObjectData } from '@google/events/cloud/storage/v1/StorageObjectData';
import { parse, write } from './csv';
import { generateMindmap } from './ai';
import { downloadFile, uploadFile } from './storage';
import { CsvInputRowData, CsvOutputRowData } from './csv/interface';
import { writeManyItemsToDb } from './db';
import { MindMapRowData } from './db/interface';

const OUTPUT_FILENAME = `output-${Date.now()}.csv`;
const OUTPUT_BUCKET = process.env.OUTPUT_BUCKET_NAME || 'mindmap-output';
const DB_COLLECTION_NAME = 'mindmap';

cloudEvent<StorageObjectData>('mindmap-process', async (cloudEvent) => {
  if (!cloudEvent.data) {
    throw new Error('No CloudEvent data');
  } else if (!cloudEvent.data.bucket) {
    throw new Error('No CloudEvent bucket data');
  } else if (!cloudEvent.data.name) {
    throw new Error('No CloudEvent filename data');
  }

  // fetch and parse the file
  let rows = [];
  try {
    await downloadFile(cloudEvent.data.bucket, cloudEvent.data.name, cloudEvent.data.name);
    rows = await parse(cloudEvent.data.name, (data: CsvInputRowData) => {
      return data.hasOwnProperty('subject') && data.hasOwnProperty('topic');
    });
    if (rows.length === 0) {
      throw new Error('CSV data could not be parsed');
    }
  } catch (e) {
    throw e;
  }

  // pass the file data to ChatGPT to create mindmap data
  const statuses: CsvOutputRowData[] = [];
  const mindmaps: MindMapRowData[] = [];
  await Promise.all(
    rows.map(async (row) => {
      try {
        const result = await generateMindmap(row.subject, row.topic);
        mindmaps.push({ key: `${row.subject}.${row.topic}`, data: result });
        statuses.push({ subject: row.subject, topic: row.topic, status: 'SUCCESS' });
      } catch (e) {
        statuses.push({ subject: row.subject, topic: row.topic, status: 'FAIL' });
      }
    })
  );

  try {
    // create the output file and save to bucket
    await write(OUTPUT_FILENAME, statuses);
    await uploadFile(OUTPUT_BUCKET, OUTPUT_FILENAME);

    // push the mindmap data to store
    await writeManyItemsToDb(DB_COLLECTION_NAME, mindmaps);
  } catch (e) {
    throw e;
  }
});
