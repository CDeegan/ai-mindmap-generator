import { Storage } from '@google-cloud/storage';
import { join } from 'path';

const storage = new Storage();

export async function uploadFile(bucket: string, filename: string) {
  await storage.bucket(bucket).upload(join(process.cwd(), filename));
}

export async function downloadFile(bucket: string, filename: string, destinationFilename: string) {
  return storage
    .bucket(bucket)
    .file(filename)
    .download({
      destination: join(process.cwd(), destinationFilename)
    });
}
