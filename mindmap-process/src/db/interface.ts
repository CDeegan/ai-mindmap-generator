import { MindMapResponse } from '../config/mindmap-ai';

export interface MindMapRowData {
  key: string;
  data: MindMapResponse;
}
