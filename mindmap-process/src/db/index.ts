import { Firestore } from '@google-cloud/firestore';
import { MindMapRowData } from './interface';

const db = new Firestore();

export async function writeManyItemsToDb(collection: string, rows: MindMapRowData[]) {
  const batch = db.batch();
  for (const row of rows) {
    const rowRef = db.collection(collection).doc(row.key);
    batch.set(rowRef, row.data);
  }
  return batch.commit();
}
