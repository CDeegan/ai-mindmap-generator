import * as csv from 'fast-csv';
import { createReadStream, createWriteStream } from 'fs';
import { join } from 'path';
import { CsvInputRowData, CsvOutputRowData } from './interface';
import { CsvParseError, CsvWriteError } from './error';

const CSV_FILE_EXTENSION = '.csv';

function isValidFileExtension(fileName: string): boolean {
  return fileName.endsWith(CSV_FILE_EXTENSION);
}

function parseCsv(fileName: string, validatorFn?: (data: CsvInputRowData) => boolean): Promise<CsvInputRowData[]> {
  const csvRows: CsvInputRowData[] = [];
  return new Promise((resolve, reject) => {
    const parser = csv.parse<CsvInputRowData, CsvInputRowData>({ headers: true });
    if (validatorFn) {
      parser.validate(validatorFn);
    }
    createReadStream(join(process.cwd(), fileName))
      .pipe(parser)
      .on('error', (error) => reject(error))
      .on('data', (row: CsvInputRowData) => csvRows.push(row))
      .on('end', () => resolve(csvRows));
  });
}

function writeCsv(filename: string, rowData: CsvOutputRowData[]): Promise<string> {
  const csvStream = csv.format();
  return new Promise((resolve, reject) => {
    csvStream
      .pipe(createWriteStream(join(process.cwd(), filename)))
      .on('error', (error) => reject(error))
      .on('end', () => resolve(filename));
    rowData.forEach((r) => csvStream.write(r));
  });
}

export async function parse(fileName: string, validatorFn?: (data: CsvInputRowData) => boolean): Promise<CsvInputRowData[]> {
  if (!isValidFileExtension(fileName)) {
    throw new Error(`${fileName} does not have a valid extension`);
  }

  try {
    return await parseCsv(fileName, validatorFn);
  } catch (e) {
    if (e instanceof Error) {
      throw new CsvParseError(`${fileName} does not have valid contents`, e);
    }
    throw e;
  }
}

export async function write(filename: string, data: CsvOutputRowData[]): Promise<void> {
  try {
    writeCsv(filename, data);
  } catch (e) {
    if (e instanceof Error) {
      throw new CsvWriteError(`Failed to write to ${filename}`, e);
    }
    throw e;
  }
}
