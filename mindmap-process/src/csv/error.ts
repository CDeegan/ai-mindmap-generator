export class CsvParseError extends Error {
  parseError: Error;
  constructor(message: string, data: Error) {
    super(message);
    this.parseError = data;
  }
}

export class CsvWriteError extends Error {
  parseError: Error;
  constructor(message: string, data: Error) {
    super(message);
    this.parseError = data;
  }
}
