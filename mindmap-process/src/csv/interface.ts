export interface CsvInputRowData {
  subject: string;
  topic: string;
}

export interface CsvOutputRowData {
  subject: string;
  topic: string;
  status: string;
}
