interface MindMapNode {
  id: number;
  text: string;
  parentId?: number;
}

export interface MindMapResponse {
  data: MindMapNode[];
  diagram: string;
}

export const config = {
  parser: {
    data: 'A JSON array of nodes with parent references representing the mindmap. Each node includes the properties "id", "text" and "parentId".',
    diagram: `A JSON string representation of the mindmap formatted in Mermaid's mindmap diagram syntax. For example:
    mindmap
      root(Java Streams)
        Stream Creation
          From Collection
          From Arrays
          From I/O Channels
        Stream Operations
          Intermediate Operations
            Filter
            Map
            FlatMap
          Terminal Operations
            ForEach
            Reduce
            Collect`
  },
  prompt: [
    ['system', 'You are a professional teacher in {subject}'],
    [
      'human',
      `
        Your goal is to generate a mind map for the subject above with the focus on the {topic} so that a student can improve their understanding of {subject} while using that mind map.
        The root node should be {subject} and it's child nodes should be {topic}. Subsequent children should include sub-topics of {topic} of your choice.
        {format_instructions}
      `
    ]
  ]
};
